/**
 * Decode function is the main entry point in the script,
 * and you should not delete it.
 *
 * Arguments:
 * 'payload' - an array of bytes, raw payload from sensor
 * 'port' - an integer value in range [0, 255], f_port value from sensor
 */

/*function for reversing the string*/
function reverseString(str) {
    return str.match(/.{1,2}/g).reverse().join("");
}

function paddedStr(padStr, userStr) {
    if (userStr.length < padStr.length) {
        return (padStr + userStr).slice(-padStr.length);
    } else {
        return (userStr + padStr).substring(0, padStr.length);
    }
}

var hexPayload = '';
var result = new Object();

function decode(payload, port) {
    Object.keys(payload).forEach(function(key) {
        thishex = Number(payload[key]).toString(16);
        thishex = thishex.length < 2 ? "" + "0" + thishex : thishex;
        hexPayload += thishex;
    });
    var packetType = parseInt(hexPayload.substring(0, 1), 16).toString(10);
    var packetSubType = parseInt(hexPayload.substring(1, 2), 16).toString(10);
    if (packetType == 1) {
        result.packetType = 'SP1: dailyValue, singleChannel';
        result.packetSubType = '1';
        result.dailyValueLiters = parseInt(reverseString(hexPayload.substring(2)), 16).toString(10);
        result.RawPayload = hexPayload;
        result.port = port;
    } else if (packetType == 2) {
        result.packetType = 'SP2: monthlyValueBeginning, singleChannel';
        result.packetSubType = '1';
        var timeBinary = (parseInt(reverseString(hexPayload.substring(2, 10)), 16).toString(2))
        var paddedBinaryTime = paddedStr('00000000000000000000000000000000', timeBinary);
        var yearBinary = paddedBinaryTime.substring(0, 4) + paddedBinaryTime.substring(8, 11);
        var yearBin2Dec = parseInt(yearBinary, 2).toString(10);
        var monthBin2Dec = parseInt(paddedBinaryTime.substring(4, 8), 2).toString(10);
        var dayBin2Dec = parseInt(paddedBinaryTime.substring(11, 16), 2).toString(10);
        var hourBin2Dec = parseInt(paddedBinaryTime.substring(19, 24), 2).toString(10);
        var minBin2Dec = parseInt(paddedBinaryTime.substring(26), 2).toString(10);
        result.timeStamp = yearBin2Dec + '/' + monthBin2Dec + '/' + dayBin2Dec + ';' + hourBin2Dec + ':' + minBin2Dec;
        var sp2Liter = parseInt(reverseString(hexPayload.substring(10, 18)), 16).toString(10);
        result.monthlyValue = sp2Liter;
        result.RawPayload = hexPayload;
        result.port = port;
    } else if (packetType == 3) {
        result.packetType = 'SP3: monthlyValueBeginning&Mid, singleChannel';
        result.packetSubType = '1';
        var timeBinary = (parseInt(reverseString(hexPayload.substring(2, 6)), 16).toString(2));
        var paddedBinaryTime = paddedStr('0000000000000000', timeBinary);
        var yearBinary = paddedBinaryTime.substring(0, 4) + timeBinary.substring(8, 11);
        var yearBin2Dec = parseInt(yearBinary, 2).toString(10);
        var monthBin2Dec = parseInt(paddedBinaryTime.substring(4, 8), 2).toString(10);
        var dayBin2Dec = parseInt(paddedBinaryTime.substring(11, 16), 2).toString(10);
        result.timeStamp = yearBin2Dec + '/' + monthBin2Dec + '/' + dayBin2Dec;
        var sp3LiterMonthly = parseInt(reverseString(hexPayload.substring(6, 14)), 16).toString(10);
        var sp3LiterHalfMonthly = parseInt(reverseString(hexPayload.substring(14)), 16).toString(10);
        result.monthlyValue = sp3LiterMonthly;
        result.halfMonthlyValue = sp3LiterHalfMonthly;
        result.RawPayload = hexPayload;
        result.port = port;
    } else if (packetType == 9 && packetSubType == 1){
        result.packetType = 'SP9: Current date and time';
        result.packetSubType = '1';
        var timeBinary = (parseInt(reverseString(hexPayload.substring(2, 10)), 16).toString(2));
        var paddedBinaryTime = paddedStr('00000000000000000000000000000000', timeBinary);
        var yearBinary = paddedBinaryTime.substring(0, 4) + paddedBinaryTime.substring(8, 11);
        var yearBin2Dec = parseInt(yearBinary, 2).toString(10);
        var monthBin2Dec = parseInt(paddedBinaryTime.substring(4, 8), 2).toString(10);
        var dayBin2Dec = parseInt(paddedBinaryTime.substring(11, 16), 2).toString(10);
        var hourBin2Dec = parseInt(paddedBinaryTime.substring(19, 24), 2).toString(10);
        var minBin2Dec = parseInt(paddedBinaryTime.substring(26), 2).toString(10);
        result.timeStamp = yearBin2Dec + '/' + monthBin2Dec + '/' + dayBin2Dec + '-' + hourBin2Dec + ':' + minBin2Dec;
        result.RawPayload = hexPayload;
        result.port = port;
    } else {}
    return result;
}
